
//var arregloXY = [[3,4]];

var minimos = [];
minimos[0]=["eje x","eje y"];
var arregloX = [];
var arregloY = [];
var find_arreglo =[];


//arrayGuardar = [100,23,200,50,300,19]

/*arregloXY[0]=["eje x","eje y"];
arregloXY[1]=[arrayGuardar[0],arrayGuardar[1]];
arregloXY[2]=[arrayGuardar[2],arrayGuardar[3]];
arregloXY[3]=[arrayGuardar[4],arrayGuardar[5]];*/

    function dibuja_min() {
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart_min);
    }

    function drawChart_min() {
        var dato = google.visualization.arrayToDataTable(minimos);

        var options = {
          title: 'Company Performance',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('minimos_cuadrados'));

        chart.draw(dato, options);
        document.getElementById("arreglito1").innerHTML = minimos;
        document.getElementById("arreglito2").innerHTML = find_arreglo[1];
      }

function Capturar_min(){
        let lstNumero = document.getElementsByClassName("numero"),
        arrayGuardar = [];
        var contador=0;

        //lstNumero[15,8,6,9,2,6,10,9,14,4]

        for (var i = 0; i < lstNumero.length; i++) {    
            if(i%2 == 0){
                arregloX.push(parseFloat(lstNumero[i].value));
                //15
            } else {
                arregloY.push(parseFloat(lstNumero[i].value));
                //7
            }
        } 

        find_arreglo = findLineByLeastSquares(arregloX, arregloY);

        // find_arreglo[[x0,x1,x2][y0,y1,y2]]
        for (var k = 1; k < (lstNumero.length+1) ; k++) {
            //console.log(find_arreglo.length);

            minimos[k]=[find_arreglo[0][contador],find_arreglo[1][contador]];
            
            //console.log (find_arreglo[0][contador],find_arreglo[1][contador]);
            
            contador++;
        }

        //document.getElementById("arreglito2").innerHTML = minimos;
        //document.getElementById("arreglito1").innerHTML = arregloXY;
        //document.getElementById("arreglito1").innerHTML = arregloX;
        //document.getElementById("arreglito2").innerHTML = arregloY;
        //console.log(find_arreglo[0]);

        dibuja_min();
    }

    function findLineByLeastSquares(values_x, values_y) {
        var x_sum = 0;
        var y_sum = 0;
        var xy_sum = 0;
        var xx_sum = 0;
        var count = 0;
    
        /*
         * The above is just for quick access, makes the program faster
         */
        var x = 0;
        var y = 0;
        var values_length = values_x.length;
        //console.log(values_x.length);
    
        if (values_length != values_y.length) {
            throw new Error('The parameters values_x and values_y need to have same size!');
        }
    
        /*
         * Above and below cover edge cases
         */
        if (values_length === 0) {
            return [ [], [] ];
        }
    
        /*
         * Calculate the sum for each of the parts necessary.
         */
        for (let i = 0; i< values_length; i++) {
            x = values_x[i];
            y = values_y[i];
            x_sum+= x;
            y_sum+= y;
            xx_sum += x*x;
            xy_sum += x*y;
            count++;
        }
    
        /*
         * Calculate m and b for the line equation:
         * y = x * m + b
         */
        var m = (count*xy_sum - x_sum*y_sum) / (count*xx_sum - x_sum*x_sum);
        var b = (y_sum/count) - (m*x_sum)/count;
    
        /*
         * We then return the x and y data points according to our fit
         */
        var result_values_x = [];
        var result_values_y = [];
    
        for (let i = 0; i < values_length; i++) {
            x = values_x[i];
            y = x * m + b;
            result_values_x.push(Math.round(x*100)/100);
            result_values_y.push(Math.round(y*100)/100);
        }
    
        return [result_values_x, result_values_y];
    }

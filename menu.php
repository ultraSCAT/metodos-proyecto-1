<?php 
include_once 'binario.php';

$base="10";
$opcion='vacio';

if (isset($_GET["opcion"])) {
	$opcion= $_GET["opcion"];
	$base= $_GET["base"];
}


$numero = isset($_POST["n_ingresado"]) ?
	(int) $_POST["n_ingresado"]:
	$_GET["numero"];

$binario = new Binario;
$textoInput= $numero;
switch ($opcion) {
	case 'bin':
		$resultado  = $binario->convertBin($numero);
		$textoInput = $binario->getBin($resultado);

		break;
	case 'octa':
	$textoInput = decoct( $numero );
	break;

	case 'hexa':
	$textoInput = dechex( $numero );
	break;

	default:
		break;
}

?>


<!DOCTYPE html>
<html>
<head>
	<title>Proyecto 1</title>
	<meta charset="utf-8">
	<meta lang="es">
	<link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body>
<header>
	<h1>Conversi&oacute;nes posibles para <br> <?=$textoInput?><sub><?=$base?></sub></h1>
</header>
<div>
	<button onclick="location.href='menu.php?opcion=bin&base=2&numero=<?=$numero?>'">Binario</button>
	<button onclick="location.href='menu.php?opcion=octa&base=8&numero=<?=$numero?>'">Octal</button>
	<button onclick="location.href='menu.php?opcion=hexa&base=16&numero=<?=$numero?>'">Hexadecimal</button>
	<button onclick="location.href='menu.php?opcion=dec&base=10&numero=<?=$numero?>'">Decimal</button>
</div>
	<form action="menu.php" method="POST" target="self" class="form">
		<input type="number" name="n_ingresado" required>
		<label  class="lbl-nombre" for="n_ingresado">
			<span class="text-nomb"><?=$numero?></span>
		</label>
		<input type="submit" value="Enviar">
	</form>


</body>
</html>